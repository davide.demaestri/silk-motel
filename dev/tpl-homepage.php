<!DOCTYPE html>
<html lang="it">
<head>
	<title>Silk Motel</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include('inc/styles.inc.php');?>
</head>

<body class="home">
	<div id="page">
	
		<?php include('inc/header.inc.php');?>

		<div class="intro-slider">
			<div class="slick-slider">
				<div class="item-slide">
					<picture class="img-slide">
					    <source srcset="https://via.placeholder.com/1920x800" media="(min-width: 768px)" />
					    <img srcset="https://via.placeholder.com/800x600" alt="Sample pic" />
					</picture>
					<div class="payoff bottom"><div class="txt">Provami una volta lorem ipsum dolor sit amet</div></div>
				</div>
				<div class="item-slide">
					<picture class="img-slide">
					    <source srcset="https://via.placeholder.com/1920x800" media="(min-width: 768px)" />
					    <img srcset="https://via.placeholder.com/800x600" alt="Sample pic" />
					</picture>
					<div class="payoff bottom"><div class="txt">Provami una volta lorem ipsum dolor sit amet</div></div>
				</div>
			</div>
		</div>

		<main id="content" class="site-content">
			<section class="area-camere">
				<div class="intro-section">
					<div class="main-wrapper">
						<h2 class="intro-title alcenter">Le nostre camere</h2>
						<div class="intro-text alcenter">Uniche ed esclusive come solo il Silk Motel sa essere</div>
					</div>
				</div><!--/.intro-section-->
				<div class="listing-camere-slider alternate"><!--/alternate genera alternanza sx/dx sul listato-->
					<div class="item-camera" data-slider="1">
						<div class="item-text">
							<div class="main-wrapper flex-wrapper">
								<div class="content">
									<div class="pre-title">Un'esperienza unica</div>
									<h3 class="title with-icon"><img src="app/images/icon-crown.svg" alt=""><span>Artic Super Suite SPA</span></h3>
									<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</div>
									<div class="readmore"><a href="#" class="cta-default medium">Scopri</a></div>
								</div><!--/.content-->
							</div><!--/.main-wrapper-->
						</div><!--/.item-text-->
						<div class="item-slider">
							<picture class="img-slide">
							    <source srcset="app/images/fake-img-01.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-01.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
							<picture class="img-slide">
							   	<source srcset="app/images/fake-img-02.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-02.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
						</div>
						<div class="item-arrows"></div>
					</div><!--/.item-camera-->
					<div class="item-camera" data-slider="2">
						<div class="item-text">
							<div class="main-wrapper flex-wrapper">
								<div class="content">
									<div class="pre-title">Un'esperienza unica</div>
									<h3 class="title with-icon"><img src="app/images/icon-crown.svg" alt=""><span>Artic Super Suite SPA</span></h3>
									<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</div>
									<div class="readmore"><a href="#" class="cta-default medium">Scopri</a></div>
								</div><!--/.content-->
							</div><!--/.main-wrapper-->
						</div><!--/.item-text-->
						<div class="item-slider">
							<picture class="img-slide">
							    <source srcset="app/images/fake-img-02.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-02.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
							<picture class="img-slide">
							   	<source srcset="app/images/fake-img-01.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-01.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
						</div>
						<div class="item-arrows"></div>
					</div><!--/.item-camera-->
				</div><!--/.listing-camere-slider-->
			</section>
			<section class="area-extra">
				<div class="main-wrapper">
					<div class="col-group-row no-extgut">
						<div class="col one-half m-1-1">
							<div class="intro-section">
								<h2 class="intro-title">Le nostre migliori offerte</h2>
								<div class="intro-text">Lorem ipsum dolor sit amet bla bla bla....</div>
							</div><!--/.intro-section-->
							<div class="listing-offerte">
								<div class="col-group-row no-extgut">
									<div class="col one-half s-1-1">
										<a href="#" class="item-offerta" data-mh="item-offerta">
											<div class="entry-header">
												<div class="title">Titolo offerta silk motel lorem ipsum</div>
												<div class="subtitle">Silk Love - Due Notte per Ritrovarsi.</div>
											</div>
											<div class="price-wrap">
												a partire da <span>€ 219.00</span>
											</div>
										</a>
									</div><!--/.col-->
									<div class="col one-half s-1-1">
										<a href="#" class="item-offerta" data-mh="item-offerta">
											<div class="entry-header">
												<div class="title">Titolo offerta silk motel lorem ipsum</div>
												<div class="subtitle">Silk Love - Due Notte per Ritrovarsi. ue Notte per Ritrovarsi.</div>
											</div>
											<div class="price-wrap">
												a partire da <span>€ 219.00</span>
											</div>
										</a>
									</div><!--/.col-->
								</div>
								<div class="view-all alright"><a href="#" class="cta-default medium">Vedi tutte le offerte</a></div>
							</div>
						</div><!--/.col-->
						<div class="col one-half m-1-1">
							<div class="intro-section">
								<h2 class="intro-title">Le notizie dal nostro blog</h2>
								<div class="intro-text">Lorem ipsum dolor sit amet bla bla bla....</div>
							</div><!--/.intro-section-->
							<div class="listing-news summary">
								<a class="item-news" href="#">
									<figure><img src="app/images/fake-img-01-square.jpg" alt=""></figure>
									<div class="content">
										<div class="entry-excerpt">
											Sit news title what a magic wonderful post Lorem ipsum dolor sit 
										</div>
										<div class="readmore"><i class="fas fa-chevron-right"></i></div>
									</div><!--/.content-->
									
								</a><!--/.item-news-->
								<a class="item-news" href="#">
									<figure><img src="app/images/fake-img-01-square.jpg" alt=""></figure>
									<div class="content">
										<div class="entry-excerpt">
											Lorem ipsum dolor sit news title
										</div>
										<div class="readmore"><i class="fas fa-chevron-right"></i></div>
									</div><!--/.content-->
									
								</a><!--/.item-news-->
								<div class="view-all alright"><a href="#" class="cta-default medium">Vedi tutte le offerte</a></div>
							</div><!--/.listing-news-->
						</div><!--/.col-->
					</div>
				</div>
			</section>
		</main>

		<?php include('inc/footer.inc.php');?>
	</div>
	<?php include('inc/scripts.inc.php');?>

	<script>
		jQuery(document).ready(function($){
			$('.slick-slider').slick({
				fade: true,
				speed: 500,
				arrows: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
				autoplay: true
			});

			$('.listing-camere-slider .item-camera').each(function( index ) {
				$(this).attr('data-slider', index);
				$(this).find(".item-slider").slick({
					appendArrows: '[data-slider="' + index + '"] .item-arrows',
					fade: true,
					speed: 500,
					arrows: true,
					autoplay: true,
					prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
					nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
				});
			});
			
		});
	</script>
</body>

</html>
