function init(){
    fixedHeader();
}

function fixedHeader(){
	var stickyOffset = jQuery('#masthead').offset().top;

	jQuery(window).on('scroll',function(){
		var sticky = jQuery('#masthead');
		var scroll = jQuery(window).scrollTop();

		if (scroll >= stickyOffset) {
			sticky.addClass('fixed');
			newHeight = sticky.height();
			jQuery("#page").css("padding-top",newHeight);
		} else {
			sticky.removeClass('fixed');
			jQuery("#page").css("padding-top",0);
		}
	});
}