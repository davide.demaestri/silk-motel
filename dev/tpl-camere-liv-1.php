<!DOCTYPE html>
<html lang="it">
<head>
	<title>Silk Motel</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include('inc/styles.inc.php');?>
</head>

<body class="home">
	<div id="page">
	
		<?php include('inc/header.inc.php');?>

		<div class="intro-slider">
			<div class="slick-slider">
				<div class="item-slide">
					<picture class="img-slide">
					    <source srcset="https://via.placeholder.com/1920x800" media="(min-width: 768px)" />
					    <img srcset="https://via.placeholder.com/800x600" alt="Sample pic" />
					</picture>
					<div class="payoff">
						<div class="txt">
							<p class="title"><span>Artic Super Suite SPA</span></p>
							<p>Una volta lorem ipsum dolor sit amet</p>
						</div>
					</div>
				</div>
				<div class="item-slide">
					<picture class="img-slide">
					    <source srcset="https://via.placeholder.com/1920x800" media="(min-width: 768px)" />
					    <img srcset="https://via.placeholder.com/800x600" alt="Sample pic" />
					</picture>
					<div class="payoff">
						<div class="txt">
							<p class="title"><span>Artic Super Suite SPA</span></p>
							<p>Una volta lorem ipsum dolor sit amet</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<main id="content" class="site-content">

				<div class="intro-section">
					<div class="main-wrapper">
						<h2 class="intro-title alcenter">Le nostre camere</h2>
						<div class="intro-text alcenter">Uniche ed esclusive come solo il Silk Motel sa essere</div>
					</div>
				</div><!--/.intro-section-->
				
				<div class="listing-camere-slider sticky-camere"><!--sticky camere genera variazione di layout per camera in evidenza-->
					<div class="item-camera">
						<div class="item-text">
							<div class="main-wrapper flex-wrapper">
								<div class="content">
									<h3 class="title"><i class="far fa-star"></i> Supersuite del mese</h3>
									<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>Ut enim ad minim veniam, quis nostrud exercitation</div>
									<div class="readmore">
										<a href="#" class="cta-default medium">Scopri</a> <a href="#" class="cta-default medium white">Prenota</a>
									</div>
								</div><!--/.content-->
							</div><!--/.main-wrapper-->
						</div><!--/.item-text-->
						<div class="item-slider">
							<picture class="img-slide">
							    <source srcset="app/images/fake-img-01.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-01-square.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
							<picture class="img-slide">
							   	<source srcset="app/images/fake-img-02.jpg" media="(min-width: 650px)" />
							    <img src="app/images/fake-img-02-square.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
							</picture>
						</div>
						<div class="item-arrows"></div>
					</div><!--/.item-camera-->
				</div><!--/.listing-camere-slider-->

				<div class="listing-camere-wall">
					<div class="main-wrapper">
						<article class="item-camera">
							<div class="col-group-row no-extgut space2x">
								<div class="col one-half m-1-1">
									<picture class="thumb">
									   	<source srcset="app/images/fake-img-03.jpg" media="(min-width: 768px)" /><!-- quadrata -->
									    <img src="app/images/fake-img-03-m.jpg" alt=""><!--/.mettere immagine con formato +rettangolare -->
									</picture>
								</div><!--/.col-->
								<div class="col one-half m-1-1">
									<div class="content">
										<div class="pre-title">Un'esperienza unica ed irripetibile</div>
										<h3 class="title with-icon"><img src="app/images/icon-crown.svg" alt=""><span>Artic Super Suite SPA</span></h3>
										<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</div>
										<div class="readmore">
											<a href="#" class="cta-default">Scopri</a> <a href="#" class="cta-default white">Prenota</a>
										</div>
										<table class="price-table">
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
										</table>
									</div><!--/.content-->
								</div><!--/.col-->
							</div><!--/.col-group-row-->
						</article>
						<article class="item-camera">
							<div class="col-group-row no-extgut space2x">
								<div class="col one-half m-1-1">
									<picture class="thumb">
									   	<source srcset="app/images/fake-img-03.jpg" media="(min-width: 768px)" /><!-- quadrata -->
									    <img src="app/images/fake-img-03-m.jpg" alt=""><!--/.mettere immagine con formato +rettangolare -->
									</picture>
								</div><!--/.col-->
								<div class="col one-half m-1-1">
									<div class="content">
										<div class="pre-title">Un'esperienza unica ed irripetibile</div>
										<h3 class="title with-icon"><img src="app/images/icon-crown.svg" alt=""><span>Artic Super Suite SPA</span></h3>
										<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</div>
										<div class="readmore">
											<a href="#" class="cta-default">Scopri</a> <a href="#" class="cta-default white">Prenota</a>
										</div>
										<table class="price-table">
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
											<tr>
												<td class="time">Da 0 a 4 ore</td>
												<td class="striked">140€</td>
												<td class="price">€ 99.00</td>
											</tr>
										</table>
									</div><!--/.content-->
								</div><!--/.col-->
							</div><!--/.col-group-row-->
						</article>
					</div>
				</div><!--/.listing-camere-wall-->

		</main>

		<?php include('inc/footer.inc.php');?>
	</div>
	<?php include('inc/scripts.inc.php');?>

	<script>
		jQuery(document).ready(function($){
			$('.slick-slider').slick({
				fade: true,
				speed: 500,
				arrows: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
				autoplay: true
			});
			$('.listing-camere-slider .item-slider').slick({
				appendArrows: '.sticky-camere .item-arrows',
				fade: true,
				speed: 500,
				arrows: true,
				autoplay: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
			});
			
		});
	</script>
</body>

</html>
