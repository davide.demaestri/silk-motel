<!DOCTYPE html>
<html lang="it">
<head>
	<title>Silk Motel</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include('inc/styles.inc.php');?>
</head>

<body class="home">
	<div id="page">
	
		<?php include('inc/header.inc.php');?>
		
		<div class="listing-camere-slider sticky-camere"><!--sticky camere genera variazione di layout per camera in evidenza-->
			<div class="item-camera">
				<div class="item-text">
					<div class="main-wrapper flex-wrapper">
						<div class="content">
							<div class="pre-title"><i class="far fa-star"></i> Supersuite del mese <i class="far fa-star"></i></div>
							<h3 class="title"><span>Artic Super Suite SPA</span></h3>
							<a href="https://www.youtube.com/watch?v=Li1gU7_Np90"class="show-video" data-lity>
								<i class="far fa-play-circle"></i>
								<span>Guarda il video</span>
							</a>
						</div><!--/.content-->
					</div><!--/.main-wrapper-->
				</div><!--/.item-text-->
				<div class="item-slider">
					<picture class="img-slide">
					    <source srcset="app/images/fake-img-01.jpg" media="(min-width: 650px)" />
					    <img src="app/images/fake-img-01-square.jpg" alt=""><!--/.mettere immagine con formato meno croppato-->
					</picture>
				</div>
				<div class="item-arrows"></div>
			</div><!--/.item-camera-->
		</div><!--/.listing-camere-slider-->

		<main id="content" class="site-content">
			<section class="item-camera main-camere-container">
				<div class="main-wrapper">
					<h2 class="intro-title">Caratteristiche</h2>
					<div class="entry-content">
						<div class="col-group-row no-extgut">
							<div class="col two-third m-1-1">
								<div class="features-list col-group-row no-extgut">
									<ul class="col one-half xs-1-1"><!-- fare due colonne da backend-->
										<li>Dimensione Stanza 75 mq</li>
										<li>Riproduzione di un Paesaggio naturale</li>
										<li>Letto ad Acqua Quadrato 200x200 cm.</li>
										<li>Letto Rotondo Dimensione 200 cm Diametro.</li>
										<li>Dimensione Stanza 75 mq</li>
										<li>Riproduzione di un Paesaggio naturale</li>
										<li>Letto ad Acqua Quadrato 200x200 cm.</li>
										<li>Letto Rotondo Dimensione 200 cm Diametro.</li>
										<li>Dimensione Stanza 75 mq</li>
										<li>Riproduzione di un Paesaggio naturale</li>
										<li>Letto ad Acqua Quadrato 200x200 cm.</li>
										<li>Letto Rotondo Dimensione 200 cm Diametro.</li>
									</ul>
									<ul class="col one-half xs-1-1">
										<li>Letto ad Acqua Quadrato 200x200 cm.</li>
										<li>Letto Rotondo Dimensione 200 cm Diametro.</li>
										<li>Dimensione Stanza 75 mq</li>
										<li>Per permanenze di 12 h ed oltre, Colazione Inclusa Servita Direttamente in stanza</li>
										<li>Letto ad Acqua Quadrato 200x200 cm.</li>
										<li>Letto Rotondo Dimensione 200 cm Diametro.</li>
									</ul>
								</div>
							</div>
							<div class="col one-third m-1-1 price-col">
								<table class="price-table">
									<tr>
										<td class="time">Da 0 a 4 ore</td>
										<td class="striked">140€</td>
										<td class="price">€ 99.00</td>
									</tr>
									<tr>
										<td class="time">Da 0 a 4 ore</td>
										<td class="striked">140€</td>
										<td class="price">€ 99.00</td>
									</tr>
									<tr>
										<td class="time">Da 0 a 4 ore</td>
										<td class="striked">140€</td>
										<td class="price">€ 99.00</td>
									</tr>
									<tr>
										<td class="time">Da 0 a 4 ore</td>
										<td class="striked">140€</td>
										<td class="price">€ 99.00</td>
									</tr>
									<tr>
										<td class="time">Da 0 a 4 ore</td>
										<td class="striked">140€</td>
										<td class="price">€ 99.00</td>
									</tr>
								</table>
								<div class="cta-wrap"><a href="#" class="cta-default uppercase">Disponibilità</a> <a href="#" class="cta-default uppercase">Prenota</a></div>
							</div>
						</div><!--/.col-group-row-->
						<div class="entry-content">
							<h2>Note</h2>
							<p>L’accesso alla struttura è consentito unicamente a persone che abbiano compiuto il 18 esimo anno d’età, dotate di documento di riconoscimento Valido Tutte le stanze sono dotate di Servizio di cortesia , Shampoo Doccia, gel intimo , sapone neutro , Cuffia per capelli  e Ciabatte monouso , set dentifricio e spazzolino , ed in esclusiva un Profilattico al Gusto Fragola per rendere ancor più piccanti i vostri incontri In tutte le Suite, Suite Spa e Supersuite sono presenti all’interno oltre a tutta la Biancheria anche Accappatoi a Nido d’ape.</p>
						</div>
					</div><!--/.content-->
				</div>
			</section><!--/.main-camere-container-->
			<section class="full-page-gallery">
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
				<a href="app/images/fake-img-03-m.jpg" class="item-gallery" data-lity><img src="app/images/fake-img-03-m.jpg"></a>
			</section><!--/.full-page-galler-->
			<section class="big-cta-area">
				<div class="medium-wrapper">
					<div class="title">Lorem ipsum sit CTA</div>
					<div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</div>
					<div class="cta-wrap">
						<a href="#" class="cta-default white medium uppercase">Richiedi Disponibilità</a> <a href="#" class="cta-default white medium uppercase">Prenota Subito</a>
					</div>
				</div>
			</section><!--/.big-cta-area-->
		</main>

		<?php include('inc/footer.inc.php');?>
	</div>
	<?php include('inc/scripts.inc.php');?>

	<script>
		jQuery(document).ready(function($){
			$('.slick-slider').slick({
				fade: true,
				speed: 500,
				arrows: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
				autoplay: true
			});
			$('.listing-camere-slider .item-slider').slick({
				appendArrows: '.sticky-camere .item-arrows',
				fade: true,
				speed: 500,
				arrows: true,
				autoplay: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
			});
			
		});
	</script>
</body>

</html>
