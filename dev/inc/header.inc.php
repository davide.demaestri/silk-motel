<div class="topper-bar">
	<div class="main-wrapper flex-wrapper">
		<div class="ct-bx"><i class="fab fa-whatsapp"></i> <span>+39 345 0352324</span></div>
		<div class="ct-bx"><i class="far fa-envelope"></i> <span>info@silkmotel.it</span></div>
		<div class="ct-bx socials">
			<a href="#"><i class="fab fa-facebook-square"></i></a>
			<a href="#"><i class="fab fa-youtube-square"></i></a>
			<a href="#"><i class="fab fa-skype"></i></a>
			<a href="#"><i class="fab fa-twitter-square"></i></a>
			<a href="#"><i class="fab fa-instagram"></i></a>
		</div>
		<div class="ct-bx"><a href="#" class="cta-default">Prenota Ora</a></div>
	</div>
</div>
<header id="masthead" class="site-header">
	<div class="main-wrapper flex-wrapper">
		<div class="main-logo"><a href="#"><img src="app/images/silk-logo.svg" alt="Silk Motel"></a></div>
		<div class="site-navigation">
			<ul>
				<li class="current-menu-item"><a href="#">Home</a></li>
				<li><a href="#">Camere</a></li>
				<li><a href="#">Offerte</a></li>
				<li><a href="#">Servizi</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">Contatti</a></li>
			</ul>
		</div>
		<div id="pull"><i class="fas fa-bars"></i></div>
	</div>
</header>
