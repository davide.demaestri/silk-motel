<picture class="img-page-separator">
    <source srcset="app/images/hp-section-separator.jpg" media="(min-width: 650px)" />
    <img srcset="app/images/hp-section-separator-mobile.jpg" alt="Sample pic" />
</picture>
<footer id="colophon" class="site-footer">
	<div class="main-wrapper">
		<div class="col-group-row no-extgut">
			<div class="col one-fourth m-1-2 xs-1-1">
				<div class="widget-footer">
					<div class="widget-title">Contatti</div>
					<div class="widget-text">
						<p>Via del Commercio, 4<br>San Martino In Strada</p>
						<p>T. +39 0371.799.160<br>info@silkmotel.it<br>Skype: Silk Motel</p>
						<p>P.IVA 06608910961</p>
					</div>
				</div><!--/.widget-footer-->
			</div><!--/.col-->
			<div class="col one-fourth m-1-2 xs-1-1">
				<div class="widget-footer">
					<div class="widget-title">Come raggiungerci</div>
					<div class="widget-text">
						<p>Da Milano:<br>Autostrada A1<br>prima uscita per Lodi, a soli 7 minuti dal Motel</p>
						<p>Per informazioni su come arrivare al Motel da Bergamo,,da Torino, da Como o da Varese, segui le mappe che trovi in basso a questa pagina.</p>
					</div>
				</div><!--/.widget-footer-->
			</div><!--/.col-->
			<div class="col one-fourth m-1-2 xs-1-1">
				<div class="widget-footer">
					<div class="widget-title">Le nostre camere</div>
					<div class="widget-text">
						<ul>
							<li><a href="#">Supersuite</a></li>
							<li><a href="#">Suite</a></li>
							<li><a href="#">Suite Spa</a></li>
							<li><a href="#">Deluxe Dungeon</a></li>
							<li><a href="#">Offerte</a></li>
						</ul>
					</div>
				</div><!--/.widget-footer-->
			</div><!--/.col-->
			<div class="col one-fourth m-1-2 xs-1-1">
				<div class="widget-footer">
					<div class="widget-title">Tripadvisor</div>
					<div class="widget-text">
						<a href="https://www.tripadvisor.it/Hotel_Review-g3365472-d3342128-Reviews-Silk_Motel-San_Martino_In_Strada_Province_of_Lodi_Lombardy.html" target="_blank"><img src="app/images/tripadvisor-2015.png" alt=""></a>
					</div>
				</div><!--/.widget-footer-->
			</div><!--/.col-->
		</div>
	</div><!--/.main-wrapper-->
</footer>
<div class="site-closure">
	<div class="closure-logo"><span><img src="app/images/silk-logo.svg" alt="Silk Motel"></span></div>
	<div class="map-places-list"><a href="#">Da Milano</a> - <a href="#">Da Piacenza</a> - <a href="#">Da Varese</a> - <a href="#">Da Como</a> - <a href="#">Da Bergamo</a> - <a href="#">Da Torino</a></div>
</div>
